package ru.goloshchapov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.api.IPropertyService;

public interface ServiceLocator {

    @NotNull ICommandService getCommandService();

    @NotNull IPropertyService getPropertyService();

}
