package ru.goloshchapov.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Project;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIndexRemoveCommand extends AbstractProjectCommand{

    @NotNull public static final String NAME = "project-remove-by-index";

    @NotNull public static final String DESCRIPTION = "Remove project by index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        @Nullable final Project project = endpointLocator.getTaskProjectEndpoint()
                .removeProjectByIndexWithTask(session, index);
        if (project == null) throw new ProjectNotFoundException();
    }
}
