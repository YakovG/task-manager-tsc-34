package ru.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull public final String NAME = "exit";

    @NotNull public final String DESCRIPTION = "Close application";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }
}
