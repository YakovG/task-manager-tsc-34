package ru.goloshchapov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.endpoint.Session;
import ru.goloshchapov.tm.endpoint.Task;
import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByIndexFinishCommand extends AbstractTaskCommand{

    @NotNull public static final String NAME = "task-finish-by-index";

    @NotNull public static final String DESCRIPTION = "Finish task by index";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @Nullable Session session = endpointLocator.getSession();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        @Nullable final Task task = endpointLocator.getTaskEndpoint().finishTaskByIndex(session, index);
        if (task == null) throw new TaskNotFoundException();
    }
}
