package ru.goloshchapov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.bootstrap.Bootstrap;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public final class FileScanner implements Runnable{

    @NotNull
    private static final String DIRECTORY_NAME = "./";

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    private static final int INTERVAL = 5;

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final Collection<String> commands = new ArrayList<>();

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        commands.addAll(bootstrap.getCommandService().getListArgCommandNames());
        start();
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void run() {
        @NotNull final File file = new File(DIRECTORY_NAME);
        Arrays.stream(file.listFiles())
                .filter(File::isFile).collect(Collectors.toList())
                .stream()
                .filter(item -> commands.contains(item.getName()))
                .forEach(item -> {bootstrap.parseCommand(item.getName()); item.delete();});
    }

}
