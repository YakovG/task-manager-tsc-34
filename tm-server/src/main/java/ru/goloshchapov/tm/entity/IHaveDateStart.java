package ru.goloshchapov.tm.entity;

import java.util.Date;

public interface IHaveDateStart {

    Date getDateStart ();

    void setDateStart (Date date);

}
