package ru.goloshchapov.tm.exception.empty;

import ru.goloshchapov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
