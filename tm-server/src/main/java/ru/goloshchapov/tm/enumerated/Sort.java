package ru.goloshchapov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.goloshchapov.tm.comparator.*;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    DATE_FINISH("Sort by finish date", ComparatorByDateFinish.getInstance());

    @NotNull private final String displayName;

    private final Comparator comparator;

    Sort(@NotNull final String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
