package ru.goloshchapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ISessionRepository;
import ru.goloshchapov.tm.model.Session;

import java.util.List;
import java.util.function.Predicate;

public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public Predicate<Session> predicateByUserId (@NotNull final String userId) {
        return session -> userId.equals(session.getUserId());
    }

    @NotNull
    @Override
    public Session merge(@NotNull final Session session) {
        list.add(session);
        return session;
    }

    @Override
    public boolean contains(@NotNull final String sessionId) {
        return (findOneById(sessionId) != null);
    }

    @Override
    public void exclude(@NotNull final Session session) {
        list.remove(session);
    }

    @Nullable
    @Override
    public Session findByUserId(final String userId) {
        return findAll().stream()
                .filter(predicateByUserId(userId))
                .limit(1).findFirst()
                .orElse(null);
    }

    @Override
    public List<Session> findAllSession() {
        return list;
    }
}
